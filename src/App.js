import AxiosLibrary from "./components/AxiosLibrary";
import FetchApi from "./components/FetchApi";


function App() {
  return (
    <div>
      <FetchApi/>
      <hr></hr>
      <AxiosLibrary/>
    </div>
  );
}

export default App;
